package com.rave.shibe.view.shibelist

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.shibe.R
import com.rave.shibe.databinding.ItemShibeBinding
import com.rave.shibe.model.local.entity.Shibe

class ShibeAdapter(
    private val shibes: List<Shibe>
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val url = shibes[position]
        holder.loadShibeImage(url)
    }

    override fun getItemCount() = shibes.size

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(shibe: Shibe) {
            binding.ivShibe.load(shibe.url)
            var clicked = false
            binding.ivThumb.setOnClickListener {
                if (!clicked) {
                    clicked = true
                    it.setBackgroundColor(Color.MAGENTA)
                } else {
                    clicked = false
                    it.setBackgroundColor(Color.TRANSPARENT)
                }


//                it.setBackgroundResource(R.drawable.thumb_purple_foreground)
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}
