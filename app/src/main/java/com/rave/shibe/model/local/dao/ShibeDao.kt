package com.rave.shibe.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rave.shibe.model.local.entity.Shibe

@Dao
interface ShibeDao {

    @Query("SELECT * FROM shibe")
    suspend fun getAll(): List<Shibe>

    @Query("SELECT * FROM shibe WHERE favorite = 1")
    suspend fun getFavorites(): List<Shibe>

    @Insert
    suspend fun insert(shibe: List<Shibe>)

}